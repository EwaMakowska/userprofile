const webpack = require('webpack');
var path = require('path');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const config = require('./webpack.config.js');
module.exports = merge(config, {
    devtool: 'cheap-module-source-map',
    entry: './index.js',
    output: {
        path: __dirname + "/dist",
        publicPath: "/dist/",
        filename: "bundle.js"
    },
    plugins: [
        new UglifyJSPlugin({
            sourceMap: true
    }),
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('production')
            }    
        })
    ]
});