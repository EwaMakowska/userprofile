const merge = require('webpack-merge');
const common = require('./webpack.config.js');

module.exports = merge(common, {
   devtool: 'eval',
   devServer: {
    contentBase: './',   
    inline: true,
    port: 8080
   }
});
