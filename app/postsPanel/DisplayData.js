import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import './postsPanel.scss';

const DisplayData = ({ postsData}) => {

    const display = () => {
        let display = postsData.map((val, i) => {
            return ([
                <div key={'header'+i} className="postHeader">
                    <img key={'avatar'+i} src={val.avatar} />
                    <p key={'user'+i}>{val.user}</p>
                    <p key={'date'+i}>{moment([val.year, val.month-1, val.day]).fromNow()}</p>
                </div>,
                <div key={'body'+i} className="postBody">
                    <p key={'content'+i}>{val.content}</p>
                </div>
            ]);
        });
        return display;
    };
 
    return (           
        <div
            className="postsContent"
            id="postsContent">            
            {display()}
        </div>
    );
};

export default DisplayData;

DisplayData.propTypes = {
    postsData: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.object
    ])
};