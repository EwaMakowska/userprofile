import React from 'react';
import PropTypes from 'prop-types';

import './postsPanel.scss';

export default class FunctionPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            placeholder: 'Add a comment',
            value: ''
        };
            
        this.handleOnChange = this.handleOnChange.bind(this);
        this.clearTextarea = this.clearTextarea.bind(this);
    }

    handleOnChange(e) {
        e.preventDefault();
        this.setState({ value: e.target.value });
    }
    
    clearTextarea() {
        this.setState({value: ''});
    }
    
    render () {
        return (
            <div className="postsPanelFooter">
                <textarea
                    placeholder={this.state.placeholder}
                    onChange={this.handleOnChange}
                    value = {this.state.value}
                />
                <button
                    onClick={() => { this.props.setNewPost(this.state.value), this.clearTextarea(); }}
                >
                    ADD
                </button>
            </div>
        );
    }
}

FunctionPanel.propTypes = {
    setNewPost: PropTypes.func.isRequired
};
