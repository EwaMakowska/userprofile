import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import DisplayData from './DisplayData';
import FunctionPanel from './FunctionPanel';

import './postsPanel.scss';

export default class PostsPanel extends React.Component {
    constructor() {
        super();
        this.state = { postsData: [] };
                
        this.setNewPost = this.setNewPost.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({ postsData : props.postsData });
    }

    setNewPost(val) {
        let i = this.state.postsData.length;
        let newPost = {day: moment().date(), month: moment().month()+1, year: moment().year(), avatar: './fakeAvatars/defaultAvatar.svg', user: 'Annymous', content: val};
        
        const scrollDown = () => { 
            let postsContent = document.getElementById('postsContent');
            postsContent.scrollTop=postsContent.scrollHeight;
        };
      
        if(!val || val === undefined) {
            alert('Please, input some text');
        } else {
            let temp = this.state.postsData;
            temp[i] = newPost;
            this.setState({ postsData : temp }, () => { scrollDown(); });
        } 
    } 
    
    render() {
        return (
            <div className="postsPanel">
                <DisplayData postsData={this.state.postsData} />
                <FunctionPanel
                    postsData={this.state.postsData}
                    setNewPost={this.setNewPost}
                    placeholder="Add s comment"
                />
            </div>
        );
    }
}

PostsPanel.propTypes = {
    postsData: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};
