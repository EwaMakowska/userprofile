import React from 'react';
import { render } from 'enzyme';
import App from '../App';

describe('App', () => {
    it('should match snapshot', () => {
        const tree = render(<App />);
        expect(tree).toMatchSnapshot();
    });
});