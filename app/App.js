import React from 'react';
import axios from 'axios';

import { HeaderPanel, PostsPanel } from './index';

import './app.scss';

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            userData: Object,
            infoData: Object,
            postsData: Object
        };
    }

    componentDidMount() {
        axios.get('./data.json') 
            .then(res => {
                this.setState({ 
                    userData: res.data.userData,
                    infoData: res.data.infoData,
                    postsData: res.data.posts
                });  
            });
    }

    render() {
        return (
            <div className="app">
                <div>
                    <HeaderPanel
                        userData={this.state.userData}
                        infoData={this.state.infoData}
                    />
                </div>
                <div className="posts">
                    <PostsPanel postsData={this.state.postsData} />
                </div>            
            </div>
        );
    }
}



