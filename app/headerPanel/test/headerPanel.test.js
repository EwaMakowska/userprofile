import React from 'react';
import { shallow } from 'enzyme';

import HeaderPanel from '../index';

describe('HeaderPanel', () => {
    it('setState should set state without warning', () => {
        const wrapper = shallow(<HeaderPanel />);

        //expect(wrapper.state('user')).toBe(String);
        console.log(wrapper.state('user'))
        wrapper.setState({ user: 'Quinn Elizabeth' });
        expect(wrapper.state('user')).toBe('Quinn Elizabeth');     
    });

});


