import React from 'react';
import { render, shallow } from 'enzyme';

import DisplayData from '../DisplayData';
import FunctionPanel from '../FunctionPanel';

describe('DisplayPanel', () => {
    it('should match snapshot', () => {
        const wrapper = render(<DisplayData />);
        expect(wrapper).toMatchSnapshot();
    });
});

describe('FunctionPanel', () => {
    it('component should have one button', () => {
        const wrapper = shallow(<FunctionPanel />);
        expect(wrapper.find('button').length).toBe(1);
    });
});


