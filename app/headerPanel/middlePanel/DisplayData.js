import React from 'react';
import PropTypes from 'prop-types';

import './middlePanel.scss';

const DisplayData = ({ like, following, followers  }) => {

    return (
        <div className="middleDisplayData">
            <div>
                <p>{like}</p>
                <p>Liks</p>
            </div>
            <div>
                <p>{following}</p>
                <p>Following</p>
            </div>
            <div>
                <p>{followers}</p>
                <p>Followers</p>
            </div>
        </div>
    );
};

export default DisplayData;

DisplayData.propTypes = {
    like: PropTypes.number,
    following: PropTypes.number,
    followers: PropTypes.number
};