import React from 'react';
import PropTypes from 'prop-types';

const FunctionPanel = ({ setFollowers, followers }) => {

    return (
        <div className="middleFunctionPanel">
            <button
                onClick={() => setFollowers(followers+1)}
            >
                FOLLOW
            </button>
        </div>
    );
};

export default FunctionPanel;

FunctionPanel.propTypes = {
    setFollowers: PropTypes.func.isRequired,
    followers: PropTypes.number
};