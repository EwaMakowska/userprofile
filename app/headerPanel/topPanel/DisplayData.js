import React from 'react';
import PropTypes from 'prop-types';

import './topPanel.scss';

const DisplayData = ({ avatar, user, city, country }) => {

    return (
        <div className="topDisplayData"> 
            <img src={avatar} className="avatar"/>            
            <p>{user}</p>
            <p>{country}, {city}</p>
        </div>
    );
};

export default DisplayData;

DisplayData.propTypes = {
    avatar: PropTypes.string,
    user: PropTypes.string,
    city: PropTypes.string,
    country: PropTypes.string
};