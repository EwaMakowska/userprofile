import React from 'react';
import PropTypes from 'prop-types';

import './topPanel.scss';

const FunctionPanel = ({ setLike, like, toggleModalWindow }) => {

    return (
        <div className="topFunctionPanel">
            <div>
                <img src="./app/headerPanel/topPanel/icons/shareIcon.svg"
                    onClick={toggleModalWindow}
                />
            </div>
            <div>
                <img src="./app/headerPanel/topPanel/icons/likeIcon.svg"
                    onClick={() => setLike(like+1)}
                />
            </div>
        </div>
    );
};

export default FunctionPanel;

FunctionPanel.propTypes = {
    setLike: PropTypes.func.isRequired,
    like: PropTypes.number,
    toggleModalWindow: PropTypes.func.isRequired
};