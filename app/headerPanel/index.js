import React from 'react';
import PropTypes from 'prop-types';

import {ModalContainer, ModalDialog} from 'react-modal-dialog';

import { TopFunctionPanel, TopDisplayData } from './topPanel';
import { MiddleFunctionPanel, MiddleDisplayData } from './middlePanel';

import './headerPanel.scss';

export default class HeaderPanel extends React.Component {
    constructor() {
        super();
        this.state = {
            avatar: '',
            user: '',
            city: '',
            country: '',
            like: Number,
            following: Number,
            followers: Number,
            isOpen: false
        };
                
        this.setLike = this.setLike.bind(this);
        this.setFollowers = this.setFollowers.bind(this);
        this.toggleModalWindow = this.toggleModalWindow.bind(this);
    }
    
    componentWillReceiveProps(props) {
        this.setState({
            avatar : props.userData.avatar,
            user: props.userData.user,
            city: props.userData.city,
            country: props.userData.country,
            like : props.infoData.like,
            following: props.infoData.following,
            followers: props.infoData.followers,
        });
    }

    setLike(val) {
        this.setState({like: val});
    }
    
    setFollowers(val) {
        this.setState({followers: val});
    }

    toggleModalWindow() {
        this.setState({isOpen: !this.state.isOpen});
    }

    render() {
        return (
            <div className="headerPanel">
                {
                    this.state.isOpen &&
                <ModalContainer onClose={this.toggleModalWindow}>
                    <ModalDialog onClose={this.toggleModalWindow}>
                        <div className="modal">
                            <div> 
                                <div><h1>You share:</h1></div>
                            </div>
                            <p>{window.location.href}</p>
                        </div>
                    </ModalDialog>
                </ModalContainer>
                }
                <TopFunctionPanel
                    setLike={this.setLike}
                    like={this.state.like}
                    toggleModalWindow={this.toggleModalWindow}
                />
                <TopDisplayData
                    avatar={this.state.avatar}
                    user={this.state.user}
                    city={this.state.city}
                    country={this.state.country}
                />
                <MiddleDisplayData
                    like={this.state.like}
                    following={this.state.following}
                    followers={this.state.followers}
                />
                <MiddleFunctionPanel
                    setFollowers={this.setFollowers}
                    followers={this.state.followers}
                />
            </div>
        );
    }
}

HeaderPanel.propTypes = {
    userData: PropTypes.object,
    infoData: PropTypes.object
};
