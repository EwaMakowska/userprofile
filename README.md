# README #

There is sample of user profil.
Component use reactJS

main folder: app

demo: makowska-e.eu/sampleUserProfile

### requirements ###

node 8.0.0

### How do I get set up? ###

    * development 

git clone https://EwaMakowska@bitbucket.org/EwaMakowska/userprofile.git

cd  userProfile

npm i

npm start

start on localhost:8080

    * production

npm run build

### esLint ###

npm run lint

### unit test ###

npm run test