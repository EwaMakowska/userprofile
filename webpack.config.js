var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: './index.js',
    output: {
        path: __dirname + "/dist",
        publicPath: "/dist/",
        filename: "bundle.js"
    },
   module: {
      loaders: [
        { test: /\.js?$/, exclude: /node_modules/, loader: 'babel-loader' },
        { test: /\.css$/, exclude: /node_modules/, loader: "style-loader!css-loader" },
        { test: /\.scss$/, exclude: /node_modules/, loader: "style-loader!css-loader!sass-loader"}
      ]
   }
}
